package hor

import (
	"fmt"
	. "math"
	"os"
	"rx"
	. "rx/math"

	"github.com/go-gl/glfw/v3.1/glfw"
)

var _ = Cos

//const tileSize = 512

type TiledBackground struct {
	texs          [][]*rx.Texture
	bgtexs        [][]*rx.Texture
	dtexs         [][]*rx.Texture
	w, h          int // total size in tiles
	shx, shy      int //
	vpw, vph      int // viewport size in pixels
	z             float64
	tileSize      int
	activeLayer   int
	visible       bool
	cam           *Camera
	camInitialPos Vec3
	view          *TiledBackgroundView
}

func newTiledBackground(w, h int, vpw, vph int, cam *Camera) (tb *TiledBackground) {
	tb = &TiledBackground{
		w: w, h: h,
		vph: vph, vpw: vpw,
		tileSize: 512,
		visible:  true,
		cam:      cam,
	}

	// make texs
	tb.bgtexs = make([][]*rx.Texture, w)
	tb.dtexs = make([][]*rx.Texture, w)
	for i := range tb.bgtexs {
		tb.bgtexs[i] = make([]*rx.Texture, h)
		tb.dtexs[i] = make([]*rx.Texture, h)
	}

	// save cam initial pos
	tb.camInitialPos = cam.pos

	// set default layer
	tb.setLayer(0)
	return
}

func (tb *TiledBackground) load(path string) {
	for y := 0; y < tb.h; y++ {
		for x := 0; x < tb.w; x++ {
			//ftype := ".jpg"
			ftype := ".dds"

			// load bg texs
			fp := path + fmt.Sprintf("r_%03d_%03d.png", x, y)
			if _, err := os.Stat(fp + ftype); err == nil {
				fp += ftype
			}
			println("loading", fp)
			tex := rx.NewTextureOpt(fp, false)
			tb.bgtexs[x][y] = tex

			// load depth texs
			fp = path + fmt.Sprintf("depth_%03d_%03d.png", x, y)
			if _, err := os.Stat(fp + ftype); err == nil {
				fp += ftype
			}
			println("loading", fp)
			tex = rx.NewTextureOpt(fp, false)
			tb.dtexs[x][y] = tex
		}
	}

	//b.tex.Load()
}

func (tb *TiledBackground) spawn() {
	tb.view = newTiledBackgroundView(tb)
	tb.view.Spawn()
}

func (tb *TiledBackground) setLayer(n int) {
	switch n {
	case 0:
		tb.texs = tb.bgtexs
	case 1:
		tb.texs = tb.dtexs
	default:
		pp("unknown layer num:", n)
	}
}

func (tb *TiledBackground) setVisible(v bool) {
	tb.visible = v
}

// Return vp size in tiles.
func (tb *TiledBackground) vpSizeTiles() (int, int) {
	// rendering size in tiles
	sx := int(tb.vpw/tb.tileSize + 1)
	sy := int(tb.vph/tb.tileSize + 1)

	return sx, sy
}

/*
func (tb *TiledBackground) pan(x, y float64) {
	pw := 1.0 / float64(tb.vpw)
	ph := 1.0 / float64(tb.vph)
	tb.shx = int(x * pw)
	tb.shy = int(y * ph)
}
*/
func (tb *TiledBackground) update(dt float64) {

	speed := int(1000.0 * dt)
	px := 0
	py := 0

	kb := _InputSys.Keyboard
	if kb.IsKeyPressed(glfw.KeyKP8) {
		py = -speed
	} else if kb.IsKeyPressed(glfw.KeyKP2) {
		py = speed
	}
	if kb.IsKeyPressed(glfw.KeyKP4) {
		px = speed
	} else if kb.IsKeyPressed(glfw.KeyKP6) {
		px = -speed
	}

	// camera unit size in pixels
	uSizePx := 512.0 / 10.0

	posDiff := tb.camInitialPos.Sub(tb.cam.pos)
	//posDiff := tb.cam.pos.Sub(tb.camInitPos)
	ary := Radians(tb.cam.rot.Z())
	//ary := Radians(tb.cam.rot.Y())
	var _ = ary
	var _ = posDiff
	var _ = uSizePx
	/*
		px = int(posDiff.X * uSizePx)
		py = int(posDiff.Y() * uSizePx)
	*/
	//px = int(-Sin(ary) * posDiff.X * uSizePx)
	//py = int(-Cos(ary) * posDiff.Y() * uSizePx)
	//px = int(-Sin(ary) * posDiff.X * uSizePx * (vars.scrollSpeed * dt))
	//py = int(-Cos(ary) * posDiff.Y() * uSizePx * (vars.scrollSpeed * dt))
	/*
		>>> v = ob2.location.z - ob1.location.z
		>>> v/10
		0.9659257888793945
	*/
	//k := 1+ (1-0.96593)
	k := 1 + (1 - 0.9659257888793945)
	kx := 1 + (1 - 0.956612491607666)
	var _ = k
	var _ = kx
	//k = 1.0
	px = 0
	//px = int(-Sin(ary) * posDiff.X * uSizePx * k)
	//px = int(posDiff.X * uSizePx)
	//py = -int(-Cos(ary) * posDiff.Z * uSizePx)
	//py = -int(-Cos(ary) * posDiff.Z * uSizePx * k)
	//py = int(posDiff.Z * uSizePx)
	//py = int(posDiff.Z * uSizePx * k)
	//px = int(posDiff.X * kx * uSizePx)
	//p(px)
	//py = int(-Cos(ary))

	py = -int(tb.cam._moveAdj.Y() * uSizePx)
	px = -int(tb.cam._moveAdj.X() * uSizePx)
	//p(tb.cam._moveAdj.X)
	//pp(px)
	//px = -52

	if px != 0 || py != 0 {
		tb.shx = px
		tb.shy = py
	}
	/*
		// update view
		if tb.view != nil {
			tb.view.update(dt)
		}
	*/
}
