package hor

type LocationChunks map[string]LocationChunk

/**** Location ****/

type Location struct {
	id     string
	name   string
	chunks LocationChunks
	chunk  *LocationChunk // active region
	items  ItemContainer
	// entry points
	// regions, parts
}

func newLocation() (l Location) {
	l = Location{
		chunks: make(LocationChunks, 0),
		items:  newItemContainer(),
	}
	return
}

/**** LocationChunk ****/

type LocationChunk struct {
	id      string
	name    string
	env     *Environment // link to ennvironment
	visited bool
	//children LocationRegions
}

func newLocationChunk() (lc LocationChunk) {
	lc = LocationChunk{}
	return
}
