package hor

import (
	//. "fmt"
	//. "math"
	"rx"
	. "rx/math"
)

type Scene struct {
	game   *Game
	camera *Camera
	light  *rx.LightNode
}

func NewScene(g *Game) (s *Scene) {
	s = &Scene{
		game: g,
	}
	s.setDefaultScene()
	return s
}

func (s *Scene) setDefaultScene() {
	/*
		Add camera
	*/
	s.camera = NewCamera()
	s.camera.SetDefaultCamera()

	rxi := rx.Rxi()
	sce := rxi.Scene()

	/*
		Add light
	*/
	l1 := sce.AddLightNode("light1")
	l1.SetPos(Vec3{10, 10, 10})
	l1.SetIntensity(0.33)
	println("l1 pos:", l1.Pos().String())

	s.light = l1
}

func (s *Scene) Update(dt float64) {
	s.camera.Update(dt)
}
