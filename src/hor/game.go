package hor

import (
	//. "fmt"
	//. "math"
	//"ltest"
	"rx"
)

var (
	game      *Game = nil
	_InputSys *rx.InputSys
	_Scene    *Scene
)

func initNewGame(app *rx.App) bool {
	_ = newGame(app)
	return true
}

type Game struct {
	app      *rx.App
	timer    *Timer
	inputsys *rx.InputSys
	state    *State
	scene    *Scene
}

func newGame(app *rx.App) *Game {
	if game != nil {
		return game
	}

	println("game.new")
	SetDefaultVars()
	g := &Game{
		app:   app,
		timer: NewTimer(true),
	}
	g.inputsys = rx.NewInputSys(g.app)
	g.state = newState()
	g.scene = NewScene(g)

	// set default game
	g.setDefaultGame()

	// set access vars
	_InputSys = g.inputsys
	_Scene = g.scene

	game = g
	return game
}

func (g *Game) setDefaultGame() {
	g.scene.setDefaultScene()
	g.state.setDefaultState()
}

/*
func game() *Game {
	return game
}
*/

func (g *Game) update(dt float64) {
	dt = dt * g.timer.Speed()
	g.timer.Update(dt)

	g.scene.Update(dt)
}
