package hor

import (
	"github.com/go-gl/gl/v2.1/gl"
	"rx"
)

type TiledBackgroundView struct {
	m *TiledBackground
}

func newTiledBackgroundView(m *TiledBackground) (t *TiledBackgroundView) {
	t = &TiledBackgroundView{
		m: m,
	}
	return
}

func (v *TiledBackgroundView) Spawn() {
	rx.Rxi().Scene().AddRenderable(v)
}

func (v *TiledBackgroundView) Destroy() {
	rx.Rxi().Scene().RemoveRenderable(v)
}

func (v *TiledBackgroundView) Render(r *rx.Renderer) {
	m := v.m

	if !m.visible {
		return
	}

	tex := m.texs[0][0]
	size := m.tileSize

	// screen center
	//cx := m.vpw/2 - size/2
	//cy := m.vph/2 - size/2
	cx := m.vpw/2 + m.shx
	cy := m.vph/2 + m.shy

	// rendering size in tiles
	//sx := int(m.vpw / size+1)
	//sy := int(m.vph / size+1)
	sx, sy := m.vpSizeTiles()

	// lower tile
	lty := int(-m.shy / size)
	ltx := int(-m.shx / size)

	for y := lty; y < lty+sy+1; y++ {
		for x := ltx; x < ltx+sx+1; x++ {

			// skip overflow tiles
			if x < 0 || x > m.w-1 ||
				y < 0 || y > m.h-1 {
				continue
			}

			tex = m.texs[x][y]

			// row shift
			rsh := -(y * size)

			if tex != nil {
				gl.Enable(gl.TEXTURE_2D)
				tex.Bind()
			}

			r.RenderQuadPx(rsh+cx+(x*size)-size/2, cy+(y*size)-size/2, size, size, 0)

			if tex != nil {
				tex.Unbind()
				gl.Disable(gl.TEXTURE_2D)
			}
		}
	}

	//r.RenderQuadPx(m.shx + cx, m.shy + cy, size, size, 0)
	//r.RenderQuadPx(m.shx + cx + size, m.shy + cy, size, size, 0)
	//r.RenderQuad(0, 0, float64(size), float64(size), 0)
	//r.RenderQuad(0, 0, 512, 512, 0)

}

func (v *TiledBackgroundView) update(dt float64) {
}
