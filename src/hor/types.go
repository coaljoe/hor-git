package hor

type UpdateableI interface {
	update(dt float64)
}
