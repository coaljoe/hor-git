package hor

import (
	. "math"
	"rx"
	. "rx/math"

	"github.com/go-gl/gl/v2.1/gl"
)

var _ = Vec3Zero

// **** TDepthCombRenderPass ****

type TDepthCombRenderPass struct {
	*rx.RenderPass
	//BgDepthTex *rx.Texture
	//BgTex *rx.Texture
	dtex *rx.Texture
	// fixme?
	combShader, fsQuadShader *rx.Shader
	fboRt                    *rx.RenderTarget
	depthRt                  *rx.RenderTarget
	bgTexRt                  *rx.RenderTarget
	bgDepthTexRt             *rx.RenderTarget
	tb                       *TiledBackground
	tcam                     *rx.CameraNode
	//tcamNode *rx.CameraNode
}

func NewTDepthCombRenderPass(tb *TiledBackground) (rp *TDepthCombRenderPass) {
	rp = &TDepthCombRenderPass{
		RenderPass: rx.NewRenderPass(),
		tb:         tb,
	}
	return
}

func (rp *TDepthCombRenderPass) Init(r *rx.Renderer) {
	// fixme?
	res_path := rx.ConfGetResPath()
	rp.combShader = rx.NewShader(res_path+"shaders/camera_fsquad_depth_combine.vs",
		res_path+"shaders/camera_fsquad_depth_combine.fs")
	rp.fsQuadShader = rx.NewShader(res_path+"shaders/camera_fsquad.vs",
		res_path+"shaders/camera_fsquad.fs")
	width, height := r.Size()
	rp.fboRt = rx.NewRenderTarget("fboRt", width, height, false)
	rp.depthRt = rx.NewRenderTarget("depthRt", width, height, true)
	rp.bgTexRt = rx.NewRenderTarget("bgTexRt", width, height, false)
	rp.bgDepthTexRt = rx.NewRenderTarget("bgDepthTexRt", width, height, false)

	// tile cam
	rp.tcam = rx.NewCameraNode()
	rp.tcam.Camera.SetupView(10, 1, 1.0, 100.0, true)
}

func (rp *TDepthCombRenderPass) Render(r *rx.Renderer) {

	cam := r.CurCam()

	//pp(rxi.Scene().renderables)
	rp.tb.setVisible(false)
	/*
	     	//r.RenderAll()
	     	//return
	       //gl.Enable(gl.TEXTURE_2D)
	     	rp.fboRt.Bind()
	     	gl.Clear(gl.COLOR_BUFFER_BIT|gl.DEPTH_BUFFER_BIT)
	     	r.RenderAll()
	     	rp.fboRt.Unbind()

	     	gl.ActiveTexture(gl.TEXTURE0)
	   	gl.BindTexture(gl.TEXTURE_2D, rp.fboRt.Tex())
	   	r.RenderFsQuad()
	   	gl.BindTexture(gl.TEXTURE_2D, 0)
	     	return
	*/

	if cam.IsActive() {

		// Render dynamic objects to fbo
		//
		rp.fboRt.Bind()

		gl.Clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT)

		r.RenderAll()

		//rp.depthRt.Unbind()
		rp.fboRt.Unbind()

		// Render bgTexRt
		//
		rp.bgTexRt.Bind()

		gl.Clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT)

		rp.tb.setVisible(true)
		rp.tb.setLayer(0)

		// tiledbackground renderable
		//tbr := rxi.Scene().renderables[0]
		//tbr.Render()
		r.RenderRenderables()

		rp.bgTexRt.Unbind()
		rp.tb.setVisible(false)

		// Render bgDepthTexRt
		//
		rp.bgDepthTexRt.Bind()

		gl.Clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT)

		rp.tb.setVisible(true)
		rp.tb.setLayer(1)

		// tiledbackground renderable
		//tbr := rxi.Scene().renderables[0]
		//tbr.Render()
		r.RenderRenderables()

		rp.bgDepthTexRt.Unbind()
		rp.tb.setVisible(false)

		// Render scene to depthRt.
		//
		rp.depthRt.Bind()

		// clear tmp_rt texture
		gl.Clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT)

		// render at center
		//r.SetViewport(rp.tb.shx + (r.Width()/2 - 512/2), rp.tb.shy + (r.Height()/2 - 512/2), 512, 512)
		/*
			r.SetViewport(r.Width()/2 - 512/2, r.Height()/2 - 512/2, 512, 512)
			rp.tcam.SetPos(cam.Pos())
			rp.tcam.SetRot(cam.Rot())
			r.SetCamera(rp.tcam)
		*/

		/*
			rp.tcam.SetPos(cam.Pos())
			rp.tcam.SetRot(cam.Rot())
			//p("old:", rp.tcam.Pos())
			//rp.tcam.MoveByVec(Vec3{10.0, 0.0, 0.0})
			//rp.tcam.MoveByVec(Vec3{10, 0.0, 0.0})
			//p("new:", rp.tcam.Pos())
			r.SetViewport(r.Width()/2 - 512/2, r.Height()/2 - 512/2, 512, 512)
			//r.SetViewport(100, 100, 512, 512)
			//r.SetViewport(512 + r.Width()/2 - 512/2, r.Height()/2 - 512/2, 512, 512)
			r.SetCamera(rp.tcam)
			r.RenderAll()

			rp.tcam.MoveByVec(Vec3{10, 0.0, 0.0})
			//rp.tcam.SetPos(Vec3{-4.611777305603027, -49.08281326293945, 13.023395538330078})
			r.SetViewport(512 + r.Width()/2 - 512/2, r.Height()/2 - 512/2, 512, 512)
			r.SetCamera(rp.tcam)
			r.RenderAll()
		*/

		var _ = `
		//cx := rp.tb.vpw/2
		//cy := rp.tb.vph/2
		cx := rp.tb.vpw/2 + (rp.tb.shx)
		cy := rp.tb.vph/2 + (rp.tb.shy)
		size := rp.tb.tileSize

		//rp.tcam.SetPos(cam.Pos())
		rp.tcam.SetRot(cam.Rot())
		rp.tcam.SetPos(Vec3{-14.17790, -46.17058, 12.93307}) // origin
		r.SetCamera(rp.tcam)
		//p("old:", rp.tcam.Pos())
		//rp.tcam.MoveByVec(Vec3{10.0, 0.0, 0.0})
		//rp.tcam.MoveByVec(Vec3{10, 0.0, 0.0})
		//p("new:", rp.tcam.Pos())

		// size in screen coords

		//r.SetViewport(cx - size/2, cy - size/2, size, size)
		//r.SetViewport(cx - size/2, cy - size/2, size, size)
		//r.SetViewport(cx - size/2, cy - size/2, size + rp.tb.shx, size + rp.tb.shy)
		/*
		ofy := 0
		if((cy - size/2) < 0) {
			ofy = cy - size/2
			p(ofy)
		}
		*/
		//r.SetViewport(cx - size/2, cy - size/2, size, size + ofy)
		r.SetViewport(cx - size/2, cy - size/2, size, size) 
		//r.SetViewport(cx - size/2, -100+cy - size/2, size, size)
		//r.SetViewport(100, 100, 512, 512)
		//r.SetViewport(512 + r.Width()/2 - 512/2, r.Height()/2 - 512/2, 512, 512)
		//r.SetCamera(rp.tcam)
		r.RenderAll()

		// misc
		r.SetViewport(0, 0, 100, 100)
		r.RenderAll()

		r.SetViewport(200, 200, 100, 100)
		r.RenderAll()


		// upper
		//rp.tcam.SetPos(Vec3{-12.59058, -6.61842, 12.93889})
		rp.tcam.MoveByVec(Vec3{-10, 10.0, 0.0})
		correct_height(rp.tcam)
		r.SetCamera(rp.tcam)
		//r.SetViewport((cx - size/2), 100 + (cy - size/2), size, size)
		//r.SetViewport(0, 200 + (cy - size/2), size, size)
		r.SetViewport(-size + (cx - size/2), size + (cy - size/2), size, size)
		r.RenderAll()
`

		//var _=`
		//rp.tcam.SetPos(cam.Pos())
		rp.tcam.SetRot(cam.Rot())
		//rp.tcam.SetPos(rp.tb.cam.pos)
		//rp.tcam.SetRot(rp.tb.cam.rot)
		//tcam_origin := Vec3{-14.17790, -46.17058, 12.93307} // origin yxz
		//tcam_origin := Vec3{-14.177902221679688, -46.17058181762695, 12.933069229125977} // origin yxz precise
		tcam_origin := Vec3{-12.936243057250977, -46.53374099731445, 12.933069229125977} // origin yxz converted precise
		rp.tcam.SetPos(tcam_origin)
		r.SetCamera(rp.tcam)

		step := 10.0
		size := rp.tb.tileSize

		// skip x, y tiles from origin
		skipY := Floor(rp.tb.cam._moveAdj.Y() / 10)
		skipX := Floor(rp.tb.cam._moveAdj.X()/10) + (-skipY * 1)
		//skipY = 1
		var _ = skipX
		var _ = skipY
		/*
			// do z correction
			// note: must be always at x = 0 (+y*-1)
			if skipY > 0 || skipX > 0 {
				// save pos
				//pos := rp.tcam.Pos()
				// move y -x
				p(rp.tcam.Pos())
				rp.tcam.MoveByVec(Vec3{step*-skipX, step*skipY, 0})
				p(rp.tcam.Pos())
				correct_height(rp.tcam)
				p(rp.tcam.Pos())
				// restore pos
				rp.tcam.MoveByVec(Vec3{step*skipX, step*-skipY, 0})
				p(rp.tcam.Pos())
				//z := rp.tcam.Pos().Z
				//rp.tcam.SetPos(Vec3{pos.X, pos.Y(), z})

				p(skipX, skipY)
				pp("derp")
			}
		*/

		//rp.tcam.MoveByVec(Vec3{step*(skipX + -skipY), step*skipY, 0})
		rp.tcam.MoveByVec(Vec3{step * skipX, step * skipY, 0})
		//rp.tcam.MoveByVec(Vec3{0, step*skipY, 0})
		//rp.tcam.MoveByVec(Vec3{-10, 10, 0})
		//rp.tb.shy = -512
		//rp.tcam.MoveByVec(Vec3{0, 10, 0})
		//correct_height(rp.tcam)

		//p(skipX, skipY)sutt
		//rp.tcam.MoveByVec(Vec3{step*float64(skipX), step*float64(skipY), 0})

		tcam_render_origin := rp.tcam.Pos()
		r.SetCamera(rp.tcam)

		//cx := rp.tb.vpw/2 + (rp.tb.shx)
		//cy := rp.tb.vph/2 + (rp.tb.shy)
		//cy := rp.tb.vph/2 + (rp.tb.shy + 512)
		cx := rp.tb.vpw/2 + (rp.tb.shx + int(skipX)*size)
		cy := rp.tb.vph/2 + (rp.tb.shy + int(skipY)*size)
		sx, sy := rp.tb.vpSizeTiles()
		//sx = 2
		//sy = 2
		for y := 0; y < sy+1; y++ {
			ysh := (y * size)
			for x := 0; x < sx+1; x++ {
				//xsh := x*size
				xsh := (-y * size) + x*size
				//var _ = xsh
				//var _ = ysh
				//r.SetViewport(r.Width()/2 - size/2, r.Height()/2 - size/2, size, size)
				//r.SetViewport(xsh + (r.Width()/2 - size/2), r.Height()/2 - size/2, size, size)
				//r.SetViewport(xsh + (r.Width()/2 - size/2), ysh + (r.Height()/2 - size/2), size, size)
				/*
					r.SetViewport(rp.tb.shx + xsh + (r.Width()/2 - size/2),
								  rp.tb.shy + ysh + (r.Height()/2 - size/2), size, size)
				*/
				//r.SetViewport(xsh + ox, ysh + oy, size, size)

				//r.SetViewport(ox, oy + (y*size) - size/2, size, size)
				//r.SetViewport(ox, oy + (y*size) - size/2, size, size)
				r.SetViewport(xsh+(cx-size/2), ysh+(cy-size/2), size, size)
				r.SetCamera(rp.tcam)
				//r.SetViewport(ox, 100, size, size)
				//r.SetCamera(rp.tcam)
				r.RenderAll()
				//p("render x, y, pos: ", x, y, rp.tcam.Pos())

				// update x
				rp.tcam.MoveByVec(Vec3{step, 0, 0})
			}

			// update y

			// return
			//rp.tcam.SetPos(tcam_origin)
			rp.tcam.SetPos(tcam_render_origin)

			// shift y -x
			rp.tcam.MoveByVec(Vec3{-step * float64(y+1), step * float64(y+1), 0})
			//rp.tcam.MoveByVec(Vec3{0.0, step*float64(y+1), 0})

			correct_height(rp.tcam)
		}
		//pp(2)
		//`

		// render scene
		//r.renderScene()
		//r.renderRenderables()
		//r.renderDebug()
		//r.RenderAll()

		rp.depthRt.Unbind()
		r.SetViewport(0, 0, r.Width(), r.Height())
		r.SetCamera(cam)

		// Render the cam.rt texture with fsquad shader.
		//
		gl.Enable(gl.TEXTURE_2D)
		rp.combShader.Bind()
		rp.combShader.PassUniform1i("fboTex", 0)
		rp.combShader.PassUniform1i("depthTex", 1)
		rp.combShader.PassUniform1i("bgTex", 2)
		rp.combShader.PassUniform1i("bgDepthTex", 3)

		gl.ActiveTexture(gl.TEXTURE0)
		gl.BindTexture(gl.TEXTURE_2D, rp.fboRt.Tex())
		gl.ActiveTexture(gl.TEXTURE1)
		gl.BindTexture(gl.TEXTURE_2D, rp.depthRt.Tex())
		//gl.BindTexture(gl.TEXTURE_2D, rp.dtex.Tex())
		gl.ActiveTexture(gl.TEXTURE2)
		//gl.BindTexture(gl.TEXTURE_2D, rp.BgTex.tex)
		gl.BindTexture(gl.TEXTURE_2D, rp.bgTexRt.Tex())
		gl.ActiveTexture(gl.TEXTURE3)
		//gl.BindTexture(gl.TEXTURE_2D, rp.BgDepthTex.tex)
		gl.BindTexture(gl.TEXTURE_2D, rp.bgDepthTexRt.Tex())

		r.RenderFsQuad()

		rp.combShader.Unbind()
		gl.BindTexture(gl.TEXTURE_2D, 0)

		// end
	}
}

// From blender version.
func correct_height(ob *rx.CameraNode) {
	// http://www.cleavebooks.co.uk/scol/calrtri.htm
	A := -0.0 //None
	B := 75.0
	C := 90.0

	//a = 12.93307
	//a = ob.location.z
	a := ob.Pos().Z()
	b := -0.0 //None
	c := -0.0 //None

	// find c
	// solve asa
	// https://www.mathsisfun.com/algebra/trig-solving-sas-triangles.html

	// find angle A
	A = 180.0 - B - C
	//print("A:", A)

	// find side c
	// a/sinA = c/sinC

	c = (a * Sin(Radians(C))) / Sin(Radians(A))
	//print("c:", c)

	// find b
	// b/sinB = c/sinC
	b = (a * Sin(Radians(B))) / Sin(Radians(A))
	//print("b:", b)

	// building small triangle

	A2 := A
	B2 := B
	C2 := C

	// target heigth
	//a2 = 5.0
	a2 := 12.93307
	b2 := -0.0 //None
	c2 := -0.0 //None

	c2 = (a2 * Sin(Radians(C2))) / Sin(Radians(A2))
	//print("c2:", c2)

	r := c - c2
	//print("r:", r)

	//# move ob
	v := Vec3{0.0, 0.0, -r}
	ob.MoveByVec(v)

	// unused
	var _ = b
	var _ = B2
	var _ = b2
}
