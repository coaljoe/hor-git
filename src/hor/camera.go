package hor

import (
	ps "lib/pubsub"
	. "math"
	"rx"
	. "rx/math"

	"github.com/go-gl/glfw/v3.1/glfw"
)

type CameraState int

const (
	CameraIdleState CameraState = iota
	CameraScrollingState
)

type Camera struct {
	cam      *rx.CameraNode
	mx, my   int
	pos, rot Vec3 // internal. fixme: add underscore?
	radius,
	rotSpeed,
	rotStep,
	tilt,
	tiltMin,
	tiltMax,
	tiltStep,
	zoom,
	zoomSpeed,
	zoomStep,
	zoomMin,
	zoomMax,
	defaultZoom,
	velocity,
	maxVelocity float64
	state    CameraState
	rotAdj   float64
	_moveAdj Vec3 // fixme
}

func NewCamera() (t *Camera) {
	t = &Camera{
		state: CameraIdleState,
	}
	t.SetDefaultCamera()
	Sub(rx.Ev_mouse_move, t.onMouseMove)
	Sub(rx.Ev_key_press, t.onKeyPress)
	Sub(rx.Ev_key_release, t.onKeyRelease)
	return
}

func (c *Camera) SetDefaultCamera() {
	c.radius = Sqrt(Pow(10, 2) + Pow(10, 2)) // diagonal
	c.rotSpeed = 60
	c.rotStep = 15 //360/16
	c.tilt = 35.264
	c.tiltMin = 20
	c.tiltMax = 60
	c.tiltStep = 10
	c.zoom = 10
	c.zoomSpeed = 1
	c.zoomStep = 1
	c.zoomMin = 1
	c.zoomMax = 10
	c.defaultZoom = 3
	//c.maxVelocity = 10
	c.maxVelocity = vars.scrollSpeed

	c.pos = Vec3{30, 30, 30}
	c.rot = Vec3{0, 0, 0}
	//c.trg = Vec3{0, 0, 0}

	// Add camera.
	//
	rxi := rx.Rxi()
	//app.rxi.camera.transform(10, 10, 10,  -35.264, 45, 0)
	cam := rxi.Camera
	cam.SetZoom(c.zoom)
	//cam.SetZoom(10)
	//cam.Pos = rx.NewVector3(10, 20, 10)
	//cam.Rot = rx.NewVector3(35.264, 45, 0)
	//cam.Pos = rx.NewVector3(0, 50, 0)
	//cam.Rot = rx.NewVector3(90, 0, 0)
	//cam.SetPos(Vec3{-5, 5, -5})
	//cam.SetPos(Vec3{5, 5, 5})
	//cam.SetPos(Vec3{30, 30, 30})
	cam.SetPos(c.pos)
	//cam.Rot = rx.NewVector3(90+35.264, 45, 0)
	//cam.Rot = rx.NewVector3(35.264, 45, 0)

	//cam.SetTarget(c.trg)

	println(cam.Pos().String())
	println(cam.Rot().String())

	c.cam = cam
}

func (c *Camera) onMouseMove(ev *ps.Event) {
	p := ev.Data.(struct{ x, y int })
	println("cam.onmousemove", p.x, p.y)
	c.mx = p.x
	c.my = p.y
}

func (c *Camera) onKeyPress(ev *ps.Event) {
	key := ev.Data.(glfw.Key)
	println("cam.onkeypress", key)
	//dt := game().app.GetDt() // fixme
	/*
		switch k {
		case vars.scrollUpKey, vars.scrollDownKey,
				vars.scrollLeftKey, vars.scrollRightKey:
			//c.pos.Y() += vars.scrollSpeed * dt
			c.velocity = c.maxVelocity
		}
	*/
	var adj, adjamt float64
	adjamt = c.rotSpeed
	if key == glfw.KeyZ {
		adj = +adjamt
	} else if key == glfw.KeyX {
		adj = -adjamt
	} else {
		return
	}
	println("adj", adj)
	c.rotAdj = adj
}

func (c *Camera) onKeyRelease(ev *ps.Event) {
	key := ev.Data.(glfw.Key)
	println("cam.onkeyrelease", key)
	switch key {
	case glfw.KeyZ, glfw.KeyX:
		c.rotAdj = 0
	default:
		return
	}
}

func (c *Camera) Update_(dt float64) {
	// update zoom
	if c.zoom != c.cam.Zoom() {
		c.cam.SetZoom(c.zoom)
	}

	// fixme
	c.cam.SetPos(c.pos)
	c.cam.SetRot(c.rot)
	//c.cam.SetTarget(c.trg)
}

func (c *Camera) Pan(x, y float64) {
	c.Translate(x, y, 0)
	correct_height(c.cam)
	c.build()
}

func (c *Camera) build() {
	c.pos = c.cam.Pos() // update c.pos
	c.cam.SetPos(c.pos)
	c.cam.SetRot(c.rot)
}

func (c *Camera) Translate(x, y, z float64) {
	c.cam.Translate(Vec3{x, y, z})
	// fixme: update _moveAdj
	c._moveAdj = c._moveAdj.Add(Vec3{x, y, z})
	c.build()
}

func (c *Camera) Update(dt float64) {
	kb := _InputSys.Keyboard
	m := _InputSys.Mouse
	sw, sh := game.app.Win().Size()
	var px, pz float64
	var vv, vh Vec3
	ary := Radians(c.cam.Rot().Y())
	if kb.IsKeyPressed(vars.scrollUpKey) ||
		m.Y < vars.scrollPadding {
		px = -Sin(ary)
		pz = -Cos(ary)
		vv = Vec3{0, 1, 0}
		c.velocity = c.maxVelocity
	} else if kb.IsKeyPressed(vars.scrollDownKey) ||
		m.Y >= sh-vars.scrollPadding {
		px = Sin(ary)
		pz = Cos(ary)
		vv = Vec3{0, -1, 0}
		c.velocity = c.maxVelocity
	}
	if kb.IsKeyPressed(vars.scrollLeftKey) ||
		m.X < vars.scrollPadding {
		px = -Sin(ary + Radians(90.0))
		pz = -Cos(ary + Radians(90.0))
		vh = Vec3{-1, 0, 0}
		c.velocity = c.maxVelocity
	} else if kb.IsKeyPressed(vars.scrollRightKey) ||
		m.X >= sw-vars.scrollPadding {
		px = Sin(ary + Radians(90.0))
		pz = Cos(ary + Radians(90.0))
		vh = Vec3{1, 0, 0}
		c.velocity = c.maxVelocity
	}
	/*
		moveAdj := Vec3{px * curVel, 0, pz * curVel}
		c.pos = c.pos.Add(moveAdj)
		c.trg = c.trg.Add(moveAdj)
	`	*/

	// update zoom
	if c.zoom != c.cam.Zoom() {
		c.cam.SetZoom(c.zoom)
	}

	// update pos
	if c.velocity > 0 {
		_, _ = px, pz
		curVel := c.velocity * dt
		moveAdj := vv.MulScalar(curVel).Add(vh.MulScalar(curVel))
		//p(moveAdj)
		//c.pos = c.pos.Add(moveAdj)
		//c.trg = c.trg.Add(moveAdj)

		// per-pixel panning adjacements
		uSizePx := 512.0 / 10.0 // unit size pixels
		pxSizeU := 10.0 / 512.0 // pixel size units
		//moveAdj.X = moveAdj.X * pxSizeU
		//moveAdj.Y() = moveAdj.Y() * pxSizeU
		//c.Pan(moveAdj.X * pxSizeU, moveAdj.Y() * pxSizeU)
		//c.Pan(Floor(moveAdj.X + .5) * pxSizeU, Ceil(moveAdj.Y()) * pxSizeU)
		//c.Pan(1 * pxSizeU, Ceil(moveAdj.Y()) * pxSizeU)
		c.Pan(
			Floor(moveAdj.X()*uSizePx+.5)*pxSizeU,
			Floor(moveAdj.Y()*uSizePx+.5)*pxSizeU)
		//p(moveAdj.X, moveAdj.X * uSizePx)
		/*
			c.cam.MoveByVec(moveAdj)
			c.pos = c.cam.Pos() // update c.pos

			c._moveAdj = c._moveAdj.Add(moveAdj)
			//p(moveAdj, c._moveAdj, c.pos)
		*/
		// reset velocity
		c.velocity = 0
	}
	c.cam.SetPos(c.pos)
	c.cam.SetRot(c.rot)
	//c.cam.SetTarget(c.trg)

	/*
	   	// update rot
	   	adjrot := false
	   	if adjrot {
	   		// adj rot
	   		new_rot := Mod(c.rot + c.rotAdj * dt, 360)
	     		c.rot = new_rot

	     		// fix: not working
	     		r := Abs(c.pos.X - c.trg.X)
	     		//p(r)
	     		aa := Radians(c.rot)
	     		new_pos := c.pos
	     		//new_pos.X = r * Sin(aa) + c.trg.X
	     		//new_pos.Z = r * Cos(aa) + c.trg.Z
	     		new_pos.X = r * Sin(aa) + c.pos.X
	     		new_pos.Z = r * Cos(aa) + c.pos.Z
	     		//c.pos = new_pos
	     		c.cam.SetPos(new_pos)
	     	}
	*/

	//p("new rot", c.rot, c.rotAdj)
}
