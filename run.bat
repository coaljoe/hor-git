@echo off
set PATH=%PATH%;lib\dll
set GOPATH=%CD%\..\go;%CD%
set GOMAXPROCS=0
set GODEBUG=efence=1

set LOGLEVEL=INFO

@rem set mingw env
rem call "C:\Program Files\mingw-w64\x86_64-5.3.0-posix-seh-rt_v4-rev0\mingw-w64.bat"
set PATH=C:\Program Files\mingw-w64\x86_64-5.3.0-posix-seh-rt_v4-rev0\mingw64\bin;%PATH%

@rem always clean
go clean -i rx

go run -v src\main.go