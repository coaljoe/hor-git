Content-Type: text/x-zim-wiki
Wiki-Format: zim 0.4
Creation-Date: 2014-02-19T22:06:18+04:00

====== История ======

http://wiki.parpg.net/Writing:Idea_dump
http://forums.parpg.net/index.php/topic,8.0.htmlc
http://wiki.parpg.net/Story_Format

===== Механика =====

Варианты:
* Конкурирующие сюжетные линии а-ля эроге

===== Пролог =====

Третья мировая была короткой и началась неизвестно почему. Менее чем за месяц было израсходовано почти все атомное оружие. И никто не смог выиграть.
Почти все города и инфраструктура были уничтожены, погибло от 20 до 30 % населения, крупные города были эвакуированы.

После войны люди укрылись в убежищах и непострадавших регионах.

Достаточно скоро начались новые проблемы в  виде новых конфликтов, проблем с продовольствием, болезней и т.п. К этому добавилось изменение климата - температура начала понижаться, началась [[+Ядерная Зима|Ядерная Зима]].
~~Вольты~~ Убежища были не приспособлены к этому и почти все они были заброшены.

Через несколько лет после конфликта осталось только 5% населения (около 300м.) которое жило в отдаленных частях.

(?) Сейчас, спустя 20 лет после войны ситуация примерно такая же.
(?) 1% населения?

Дополнения:

* ? уровень до молла + эвакуация
* ? чек. на диалоге эвакуации? нет удачи или скила добраться - game over?
