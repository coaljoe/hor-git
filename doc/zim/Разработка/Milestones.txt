Content-Type: text/x-zim-wiki
Wiki-Format: zim 0.4
Creation-Date: 2015-09-22T12:05:27+03:00

====== Milestones ======

==== Early Alpha (public) ====

v 0.1 alpha

описание:
альфа показывает интеракцию между персонажем и npc, работу уровней и итемов, интерфейса, звуков, оформлениe.
возможен небольшой игровой процесс, желательно нелинейный, ограниченный представленными локациями.
основная задача сделать начальный базовый каркас с базовым ф-ционалом который в последующем можно расщирять и детализировать.

локации:
* Молл вся внешняя часть и частично внутреняя (вход, холл2, холл, inn)
* возможно начало жд пути и жд станция
* Лодочное
* ко всем локациям нужен бэкграунд и заставка

оружие:
* поддержка melee weapons
* поддержка ranged weapons
* поддержка firearms?
* перезарядка и патроны
* экзотическое оружие?

npc:
* список npc: Камилла, Пьяница Джани, Дворник Матти, Kid, Стражник Дженна
* реакция npc на действия игрока: оборона, действия на основе репутации(?), проверка репутации (reputation check)
* функционирующий wandering ai

игровая система (rpgsys):
* критическое поражение
* полное использование модификаторов
* битвы

вожможные действия игрока:
* убить npc
* разговор с npc
* собрать/выбросить итем
* быть убитым
* использовать двери
* использовать предметы при открытии дверей (либо вручную либо автоматически, например карточку молла)
* перейти на другую локацию (например внутрь молла)
* снимать/одевать экипировку
* бартер с npc
* бартер с предметами
* бартер с продавцами, торговый бартер с различной валютой/без валюты?
* получение квеста
* получение предметов от npc

предметы:
* стартовые итемы для бэкграундов
* карточка Молла (нужна ли как итем?)
* валюта
* топор

квесты:
* нет глобальных квестов
* квесты Молла
* квесты Лодочного?

gui:
* полностью работающий профайл персонажа
* отображение игровой информации в логбоксе худа
* карта мира, или местная карта дорог для перехода с молла на другие локации
* hud

hor/rx:
* поддержка скелетной анимации (ходьба, смерть, использование, бег?)
* исправленный pathfinding и navmesh

звук:
* простая озвучка итемов
* озвучка интерфейса ноутбука (хватит и переключений страниц)
* фоновая музыка
* фоновая озвучка внутреннего молла

сюжет:
* возможно хватит стаба

детально
=======

локация молл:
* должны быть гуляющие дети, что-нибудь кричащие (а ля фолл), с которыми можно будет поговорить либо npc может отказаться говорить.
* на входе следует получить карточку молла от npc.
* npc на локации: camilla, тот кто выдает квесты или регистрирует рекрутов, ресепшн, продавцы и зав.отделами, мастера, посетители, группы вернувшиеся/отправляющиеся на квест, охранники?, обслуживающий персонал (повара, уборщики и тп), жители молла, работающие, отдыхающие... но в целом должно быть свободно, никто шататься не должен, в молле всего около 50 человек
* авт.система контроля доступа? может хватит и простых замков
* денежная система и свод правил, их проверка, система сигнализации?
* лифты - не работают
* изменить цвет вывесок на контрастный
* оптимизировать цвета для цветовой схемы

игровой процесс
=============

1. Выбор персонажа из имеющихся
2. Стартовая локация (набор для разных бэкграундов?)
3. Выход на Карту мира и отправление в одну из локаций (или автоматически в зависимости от бекграунда)
4. Молл
5. Получение квестов в Молле
6. Выполнение квестов и другие локации имеющиеся в альфе

вопросы:
* как будут работать сейвы?
* какой нужен пролог?
* ограничения Карты мира
* степень детализации локаций? может хватит процентов 20-50?
* локации должны быть переделываемыми/изменяемыми в последствии?

вопросы и проблемы
================

* язык интерфейса и квестов

  интерфейс возможно стоит делать на английском, со всеми лейблами, а текст квестов и детальных описаний на русском для дальнейшего перевода (возможно автоматического, для техдемо)

* сайт и информация

  с наличием техдемо неплохо иметь и сайт с информацией, для этого нужно иметь описание самого проекта. сайт на русском с авт.переводом. домен желательно собственный.

* лицензии на контент

  сейчас никак не представленны как и сопроводительная документация

* видеоролик с техдемо

  для ютуба, возможно для фандинга
