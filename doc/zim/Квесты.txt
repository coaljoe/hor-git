Content-Type: text/x-zim-wiki
Wiki-Format: zim 0.4
Creation-Date: 2014-02-11T17:07:40+04:00

====== Квесты ======

http://wiki.parpg.net/Quests

Тупые сайд-квесты вроде "Принеси яблок" должны быть простыми и забавными, пример:
засорившаяся цистерна с водой или первый квест с потерявшейся собакой в адоме.

**Требования:**
* Multiple ways to reach a goal and complete quests.
* Implementation of non-violent ways to solve quests.
* Influence of player character stats. 
