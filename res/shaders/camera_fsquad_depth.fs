#include "lib/fxaa.fs"
//uniform sampler2D fboTex;
uniform sampler2D depthTex;
uniform vec2 ScreenPos;
varying float depth;
//varying vec4 position_;

float LinearizeDepth(float zoverw)
{
	float n = 1.0;
	float f = 100.0;
	
	return(2.0 * n) / (f + n - zoverw * (f - n));	
}

void main()
{
/*
    vec3 color;
    //color = vec3(0.0,0.0,1.0);

    // test
    //vec3 c2 = texture2D(fboTex, ScreenPos).rgb;
    //vec3 c2 = texture2D(fboTex, gl_TexCoord[0].st).rgb;
    //color = (color + c2) / 2;
    //color = vec3(color.r, max(color.g,  color.b), color.b);

    /*color = texture2D(fboTex, gl_TexCoord[0].st).rgb;*/
    /*gl_FragColor.rgb = color;*/
    /*gl_FragColor.rgb = vec3(depth, 0.0, 0.0);*/
    /*gl_FragColor.r = depth;*/

/*
    gl_FragColor.rgb = color;

    float ndcDepth = ndcPos.z =
    (2.0 * gl_FragCoord.z - gl_DepthRange.near - gl_DepthRange.far) /
    (gl_DepthRange.far - gl_DepthRange.near);
    float clipDepth = ndcDepth / gl_FragCoord.w;
    gl_FragColor = vec4((clipDepth * 0.5) + 0.5); 
    gl_FragColor.r = depth;
*/

/*
    vec2 texCoord = gl_TexCoord[0].st;
    //vec4 tex = (texture2D( depthTex, texCoord ));
    vec4 tex = (texture2D( depthTex, texCoord ) * 1000) - vec4(999);
    float depth = tex.r;
    //vec4 tex = (texture2D( fboTex, texCoord ) * 1000) - vec4(999);
    //gl_FragColor = tex;
    //gl_FragColor = vec4(tex.x, tex.x, tex.x, tex.x);
    gl_FragColor = vec4(depth, depth, depth, 1.0);
*/

/*
    float depth = ((position_.z / position_.w) + 1.0) * 0.5;
    gl_FragColor = vec4(depth, depth, depth, 1.0);
*/


    float depth = texture2D(depthTex, gl_TexCoord[0].st).r;
    //depth = LinearizeDepth(depth)*77;
    depth = LinearizeDepth(depth)*6;

    gl_FragColor = vec4(depth, depth, depth, 1.0);

}

/* vim:set ft=glsl: */
